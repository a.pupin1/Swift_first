class NameList {
    var storage = [Character : [String]]()
    
    func addName(lastName: String) {

        guard let key: Character = lastName.first else { return }

        if var tempStorage = storage[key]  {
            tempStorage.append(lastName)
            storage[key] = tempStorage
        } else {
            storage[key] = [lastName]
        }
    }

    func printNames() {
        for key in storage.keys.sorted() {
            print(key)
            for name in storage[key]!.sorted() {
                print(name)
            }
        }
    }
}

let list = NameList()
list.addName(lastName: "Краков")
list.addName(lastName: "Бочкина")
list.addName(lastName: "Бататова")
list.addName(lastName: "Акулова")
list.addName(lastName: "Якушев")
list.addName(lastName: "Колбасиков")
list.addName(lastName: "Валерьянов")
list.printNames()