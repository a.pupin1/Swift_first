
import Foundation

//Задание 1

struct Candidate {
    enum Grade {
        case junior
        case middle
        case senior
    }
    // Грейд (уровень) кандидата
    let grade: Grade
    // Требуемая зарплата
    let requiredSalary: Int
    // Полное имя кандидата
    let fullName: String
}

protocol FilterCandidate {
    func filter(in array: [Candidate]) -> [Candidate]
}

struct FilterByGrade: FilterCandidate {
    let findGrade: Candidate.Grade
    
    func filter(in array: [Candidate]) -> [Candidate] {
        array.filter { candidate in candidate.grade == findGrade }
    }
}

struct FilterBySalary: FilterCandidate {
    let findSalary: Int
    
    func filter(in array: [Candidate]) -> [Candidate] {
        array.filter { candidate in candidate.requiredSalary <= findSalary }
    }
}

struct FilterByName: FilterCandidate {
    let findName: String
    
    func filter(in array: [Candidate]) -> [Candidate] {
        array.filter { candidate in candidate.fullName.contains(findName)
        }
    }
}

var allCandidates: [Candidate] = [
    Candidate(grade: .junior, requiredSalary: 1000, fullName: "Петров Джун Анатольевич"),
    Candidate(grade: .middle, requiredSalary: 3000, fullName: "Иванов Мидл Хасанович"),
    Candidate(grade: .senior, requiredSalary: 5000, fullName: "Макгрегор Конор Сеньерович"),
    Candidate(grade: .senior, requiredSalary: 5500, fullName: "Тайсон Майк Сеньерович")
]


var filterResultsByGrade = FilterByGrade(findGrade: .middle).filter(in: allCandidates)
result(message: "Фильтрация по грейду:", array: filterResultsByGrade)

var filterResultsBySalary = FilterBySalary(findSalary: 3500).filter(in: allCandidates)
result(message: "Фильтрация по зарплате:", array: filterResultsBySalary)

var filterResultsByName = FilterByName(findName: "Сеньерович").filter(in: allCandidates)
result(message: "Фильтрация по имени:", array: filterResultsByName)


func result(message: String, array: [Candidate]) {
    var result = [String]()
    
    for i in 0..<array.count {
        result.append(array[i].fullName)
    }
    print(message + " \(result)")
}



//Задание 2

extension Candidate.Grade {
    
    var gradeRank: Int {
        switch self {
        case .junior: return 1
        case .middle: return 2
        case .senior: return 3
        }
    }
}

struct FilterByMinGrade: FilterCandidate {
    let minGrade: Candidate.Grade
    
    func filter(in array: [Candidate]) -> [Candidate] {
        array.filter { candidate in candidate.grade.gradeRank >= minGrade.gradeRank }
    }
}

let filterResultsByMinGrade = FilterByMinGrade(minGrade: .middle).filter(in: allCandidates)
result(message: "Минимальный грейд и выше:", array: filterResultsByMinGrade)
