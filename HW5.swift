<<<<<<< HW5.1
import Foundation

//Задание 1
class Shape {

     func calculateArea() -> Double {
        fatalError("not implemented")
    }

     func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

class Rectangle: Shape {
    private let width: Double
    private let height: Double

    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }

    override func calculateArea() -> Double {
        width * height
    }

    override func calculatePerimeter() -> Double {
         2 * (width + height)
    }
}

class Circle: Shape {
    private let radius: Double

    init(radius: Double) {
        self.radius = radius
    }

    override func calculateArea() -> Double {
        return Double.pi * pow(radius, 2)
    }

    override func calculatePerimeter() -> Double {
        return 2 * Double.pi * radius
    }
}

class Square: Shape {
    private let side: Double

    init(side: Double) {
        self.side = side
    }

    override func calculateArea() -> Double {
        return pow(side, 2)
    }

    override func calculatePerimeter() -> Double {
        return 4 * side
    }
}

let rectangle = Rectangle(width: 4, height: 6)
let circle = Circle(radius: 5)
let square = Square(side: 10)

var shapes: [Shape] = [rectangle, circle, square]

var areaCount: Double = 0
var perimeterCount: Double = 0

for shape in shapes {
    areaCount += shape.calculateArea()
    perimeterCount += shape.calculatePerimeter()
}

print("Общая площадь \(shapes.count) фигур: \(areaCount))")
print("Общий периметр \(shapes.count) фигур: \(perimeterCount))")
=======
//Задание 2

func findIndexWithGenerics<T: Equatable>(valueToFind: T, in array: [T]) -> Int? {
    
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayOfInt = [1, 2, 3, 4, 5]
let arrayOfDouble = [1.0, 2.0, 3.0, 4.0, 5.0]

if let foundIndex = findIndexWithGenerics(valueToFind: "пес", in: arrayOfString) {
    print("Индекс элемента: \(foundIndex)") // Индекс элемента: 1
}

if let foundIndex = findIndexWithGenerics(valueToFind: 3, in: arrayOfInt) {
    print("Индекс элемента: \(foundIndex)") // Индекс элемента: 2
}

if let foundIndex = findIndexWithGenerics(valueToFind: 4.0, in: arrayOfDouble) {
    print("Индекс элемента: \(foundIndex)") // Индекс элемента: 3
}
>>>>>>> Contents.swift
