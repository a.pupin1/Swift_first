

//Задача 1

let milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

//Задача 2

var milkPrice = 3.0

//Задача 3

milkPrice = 4.20

//Задача 4

let milkBottleCount: Int? = 20
var profit = 0.0
profit = milkPrice * Double(milkBottleCount!)
print(profit)

//дополнительное задание

if let milkBottleCount = milkBottleCount {
    profit = milkPrice * Double(milkBottleCount)
}
//Принудительное развертывание может привести к ошибке (если там будет nil)
/*
let fuel: Int? = nil
print(fuel!)
*/

//Задача 5

var employeesList: [String] = []
employeesList.append(contentsOf: ["Марфа", "Андрей", "Петр", "Геннадий", "Иван"])

//Задача 6

var isEveryoneWorkHard = false
var workingHours = 40
workingHours = 35
if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}
print(isEveryoneWorkHard)
