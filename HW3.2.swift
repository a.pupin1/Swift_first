//Простое число — это натуральное число больше 1, у которого есть всего два делителя: единица и само это число.



func checkPrimeNumber(number: Int) -> Bool {
    guard number >= 2 else { return false }

    for i in 2 ..< number {
        if number % i == 0 {
            return false
        }
    }
    return true
}

checkPrimeNumber(number: 7) // true
checkPrimeNumber(number: 8) // false
checkPrimeNumber(number: 13) // true
