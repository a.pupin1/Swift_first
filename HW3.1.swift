  func makeBuffer() -> (String) -> Void {
    var storage = ""
    
    return { value in
        if value.isEmpty {
            print(storage)
        } else {
            storage += value
        }
    }
}

var buffer = makeBuffer()

buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("") // Замыкание использовать нужно!